import asyncio
import json
import re
import time

import pandas as pd
import requests
from bs4 import BeautifulSoup
from langchain import OpenAI
from markdownify import markdownify as md
from openai import AsyncOpenAI, OpenAI
from sklearn.utils import resample

i = 0


client = OpenAI(
    # this is also the default, it can be omitted
    api_key='EMPTY',
    base_url="http://20.124.240.6:8080/v1",
)

async_client = AsyncOpenAI(api_key='EMPTY',
                           base_url="http://20.124.240.6:8080/v1",)


async def process_batch(batch):
    tasks = []
    for element in batch:
        question = async_client.chat.completions.create(
            model='gpt-3.5-turbo',
            messages=[{"role": "user", "content": f"""I want you act as a Prompt Creator.
Your goal is to understand the #Title# and create a new prompt. The new prompt should be structured as a question asked by a human to a LLM like ChatGPT.
#Title# corresponds to a title of a blog. You should make the #Created Prompt# complex to make those famous AI systems (e.g., ChatGPT and GPT4) a bit harder to handle. The #Created Prompt# must be reasonable and must be understood and responded by humans.
The
#Title#:
Tax query: {element['headline']}
#Created Prompt#:"""}])

        tasks.append(question)
    return await asyncio.gather(*tasks)


file_name = 'conversation.json'


def update_file(questions, new_data):
    existing_data = []
    conv_data = []

    with open(file_name, 'r', encoding='utf-8') as json_file:
        existing_data = json.load(json_file)

    uid = len(existing_data)
    # Combine existing data with new data
    for r, d in zip(questions, new_data):
        final = {
            "id": f"{uid}",
            "conversations": [
                {
                    "from": "user",
                    "value": r.choices[0].message.content
                },
                {
                    "from": "assistant",
                    "value": d['data']
                }
            ]
        }
        uid += 1
        conv_data.append(final)

    combined_data = existing_data + conv_data

    # Write the combined data back to the JSON file
    with open(file_name, 'w', encoding='utf-8') as json_file:
        json.dump(combined_data, json_file, ensure_ascii=False, indent=4)


async def running():
    a = open('cleartax_new.json', 'r')
    a = json.load(a)
    batch_size = 30
    for i in range(0, len(a), batch_size):
        batch = a[i:i + batch_size]
        batch_result = await process_batch(batch)
        update_file(batch_result, batch)


#     results = await asyncio.gather(*tasks)
#     print(results)

if __name__ == '__main__':
    with open('cleartax.json', 'r') as file:
        data = json.load(file)
        start = time.time()
        loop = asyncio.get_event_loop()
        loop.run_until_complete(running())

        end = time.time()
        print(f'completed in {end-start}')
    # c = 0
    # data = json.load(file)
    # for d in data[-10:]:
    #     a = re.search(r'[^A-Za-z0-9|\s]', d['headline'])
    #     if a:
    #         c += 1
    #         continue
