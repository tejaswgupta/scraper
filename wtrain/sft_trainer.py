# coding=utf-8
# Copyright 2023 The HuggingFace Inc. team. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# https://github.com/huggingface/trl/blob/main/examples/scripts/sft_trainer.py


import os
from dataclasses import dataclass, field
from typing import Optional

import torch
from accelerate import Accelerator
from datasets import load_dataset
from money_patch import replace_llama_attn_with_flash_attn
from peft import AutoPeftModelForCausalLM, LoraConfig
from tqdm import tqdm
from transformers import (AutoModelForCausalLM, AutoTokenizer,
                          BitsAndBytesConfig, HfArgumentParser,
                          TrainingArguments)
from trl import SFTTrainer
from trl.trainer import ConstantLengthDataset

replace_llama_attn_with_flash_attn()


tqdm.pandas()


# Define and parse arguments.
@dataclass
class ScriptArguments:
    """
    The name of the Casual LM model we wish to fine with SFTTrainer
    """
    model_name: Optional[str] = field(
        default="meta-llama/Llama-2-70b-hf", metadata={"help": "the model name"})
    dataset_name: Optional[str] = field(
        default="timdettmers/openassistant-guanaco", metadata={"help": "the dataset name"}
    )
    dataset_text_field: Optional[str] = field(
        default="text", metadata={"help": "the text field of the dataset"})
    log_with: Optional[str] = field(
        default=None, metadata={"help": "use 'wandb' to log with wandb"})
    learning_rate: Optional[float] = field(
        default=2e-4, metadata={"help": "the learning rate"})
    batch_size: Optional[int] = field(
        default=16, metadata={"help": "the batch size"})
    seq_length: Optional[int] = field(
        default=1600, metadata={"help": "Input sequence length"})
    gradient_accumulation_steps: Optional[int] = field(
        default=8, metadata={"help": "the number of gradient accumulation steps"}
    )
    load_in_8bit: Optional[bool] = field(
        default=False, metadata={"help": "load the model in 8 bits precision"})
    load_in_4bit: Optional[bool] = field(
        default=True, metadata={"help": "load the model in 4 bits precision"})
    use_peft: Optional[bool] = field(
        default=True, metadata={"help": "Wether to use PEFT or not to train adapters"})
    trust_remote_code: Optional[bool] = field(
        default=True, metadata={"help": "Enable `trust_remote_code`"})
    output_dir: Optional[str] = field(
        default="output-sft-13-v2", metadata={"help": "the output directory"})
    peft_lora_r: Optional[int] = field(
        default=64, metadata={"help": "the r parameter of the LoRA adapters"})
    peft_lora_alpha: Optional[int] = field(
        default=16, metadata={"help": "the alpha parameter of the LoRA adapters"})
    logging_steps: Optional[int] = field(
        default=1, metadata={"help": "the number of logging steps"})
    use_auth_token: Optional[bool] = field(
        default=True, metadata={"help": "Use HF auth token to access the model"})
    num_train_epochs: Optional[int] = field(
        default=1, metadata={"help": "the number of training epochs"})
    # max_steps: Optional[int] = field(default=-1, metadata={"help": "the number of training steps"})
    save_steps: Optional[int] = field(
        default=100, metadata={"help": "Number of updates steps before two checkpoint saves"}
    )
    save_total_limit: Optional[int] = field(
        default=10, metadata={"help": "Limits total number of checkpoints."})
    push_to_hub: Optional[bool] = field(
        default=False, metadata={"help": "Push the model to HF Hub"})
    hub_model_id: Optional[str] = field(
        default=None, metadata={"help": "The name of the model on HF Hub"})


parser = HfArgumentParser(ScriptArguments)
script_args = parser.parse_args_into_dataclasses()[0]


# Step 1: Load the model
if script_args.load_in_8bit and script_args.load_in_4bit:
    raise ValueError(
        "You can't load the model in 8 bits and 4 bits at the same time")
elif script_args.load_in_8bit or script_args.load_in_4bit:
    quantization_config = BitsAndBytesConfig(
        load_in_4bit=True,
        bnb_4bit_use_double_quant=True,
        bnb_4bit_quant_type="nf4",
        bnb_4bit_compute_dtype=torch.bfloat16
    )
    # Copy the model to each device
    device_map = {"": Accelerator().local_process_index}
    torch_dtype = torch.bfloat16
else:
    device_map = None
    quantization_config = None
    torch_dtype = None

base_model = AutoModelForCausalLM.from_pretrained(
    script_args.model_name,
    quantization_config=quantization_config,
    device_map=device_map,
    trust_remote_code=script_args.trust_remote_code,
    torch_dtype=torch_dtype,
    use_auth_token=script_args.use_auth_token,
)

base_model.config.use_cache = False

tokenizer = AutoTokenizer.from_pretrained(
    script_args.model_name, trust_remote_code=True)
tokenizer.pad_token = tokenizer.eos_token
tokenizer.padding_side = "right"  # Fix weird overflow issue with fp16 training

dataset = load_dataset(
    'json', data_files='/home/azureuser/cloudfiles/code/Users/Tejasw/votum-ml/data/dataset_raw.jsonl', split='train', num_proc=4)
dataset = dataset.train_test_split(test_size=0.1)['test']

# train_dataset = ConstantLengthDataset(
#     tokenizer,
#     dataset,
#     formatting_func=prepare_sample_text,
#     infinite=True,
#     seq_length=script_args.seq_length,

# )
# valid_dataset = ConstantLengthDataset(
#     tokenizer,
#     dataset[1300:1700],
#     formatting_func=prepare_sample_text,
#     infinite=False,
#     seq_length=script_args.seq_length,

# )


def formatting_prompts_func(example):
    text = f"""The given input is a headline of legal news, blog, or transcript relating to Indian law, paired with a response that provides a detailed explanation or discussion about the topic. Your task is to generate a similar detailed explanation or discussion when given a similar input.
    ### Input: {example['human']}
    ### Response: {example['ai']}."""
    return text


# Step 3: Define the training arguments
training_arguments = TrainingArguments(
    output_dir=script_args.output_dir,
    num_train_epochs=1,
    per_device_train_batch_size=1,
    gradient_accumulation_steps=8,
    gradient_checkpointing=True,
    optim="paged_adamw_32bit",
    logging_steps=10,
    save_total_limit=10,
    save_strategy="steps",
    save_steps=10,
    learning_rate=2e-4,
    bf16=True,
    tf32=True,
    max_grad_norm=0.3,
    warmup_ratio=0.03,
    lr_scheduler_type="cosine",
    report_to='wandb',

)


peft_config = LoraConfig(
    task_type='CAUSAL_LM',
    r=8,
    lora_alpha=16,
    lora_dropout=0.1,  # 0.05 for 7,13B
    bias='none'
)


trainer = SFTTrainer(
    model=base_model,
    train_dataset=dataset,
    # eval_dataset=valid_dataset,
    args=training_arguments,
    max_seq_length=script_args.seq_length,
    packing=True,
    # dataset_text_field=script_args.dataset_text_field,
    peft_config=peft_config,
    tokenizer=tokenizer,
    formatting_func=formatting_prompts_func,
)

# trainer.model.get_trainable_parameters()

trainer.train()

# Step 6: Save the model
trainer.save_model(script_args.output_dir)

output_dir = os.path.join(script_args.output_dir, "final_checkpoint")
trainer.model.save_pretrained(output_dir)

# Free memory for merging weights
del base_model
torch.cuda.empty_cache()

model = AutoPeftModelForCausalLM.from_pretrained(
    output_dir, device_map="auto", torch_dtype=torch.bfloat16)
model = model.merge_and_unload()

output_merged_dir = os.path.join(
    script_args.output_dir, "final_merged_checkpoint")
model.save_pretrained(output_merged_dir, safe_serialization=True)
