import asyncio
import time

from gradio_client import Client

client = Client("http://47.103.63.15:50087/")

start = time.time()
a = client.predict(
    """I want you act as a Prompt Creator.
    Your goal is to understand each title from the given #Titles# and create new prompts corresponding to each title. The new prompts should be structured as a question asked by a human to a LLM like ChatGPT.
    You should make the prompts complex to make those famous AI systems (e.g., ChatGPT and GPT4) a bit harder to handle.
    #Titles#:
    1. What Is Minting Crypto?
    2. GST on iPhone: HSN Code and GST Rate on iPhone
    3. What is Taproot and How Will it Benefit Bitcoin?
    4. What is a wDogecoin : A Complete Overview
    #Created Prompts#:""",  # str in 'parameter_1' Textbox component
    "",  # str in 'System Prompt' Textbox component
    0.6,
    200,
    api_name="/chat"
)

end = time.time()
print("Section 1 time: {:.2f} seconds".format(end - start))
print(a)


# start = time.time()
# result = []
# for i in range(5):
#     result.append(client.submit(
#         """I want you act as a Prompt Creator.
#         Your goal is to understand the #Title# and create a new prompt. The new prompt should be structured as a question asked by a human to a LLM like ChatGPT.
#         You should make the prompt complex to make those famous AI systems (e.g., ChatGPT and GPT4) a bit harder to handle.
#         #Title#: Tax query: All About GST on Cross Charge Transactions
#         #Created Prompt#:""",  # str in 'parameter_1' Textbox component
#         "",  # str in 'System Prompt' Textbox component
#         0.4,
#         100,
#         api_name="/chat"
#     ))
# end = time.time()
# print("Section 1 time: {:.2f} seconds".format(end - start))
# print(result)

# tasks = []
# for i in range(2):
#     task = client.submit(
#         """I want you act as a Prompt Creator.
#     Your goal is to understand the #Title# and create a new prompt. The new prompt should be structured as a question asked by a human to a LLM like ChatGPT.
#     The #Title# corresponds to a title of a blog. You should make the prompt complex to make those famous AI systems (e.g., ChatGPT and GPT4) a bit harder to handle. The #Created Prompt# must be reasonable and must be understood and responded by humans.
#     #Title#:
#     Tax query: All About GST on Cross Charge Transactions
#     #Created Prompt#:
#     """,  # str in 'parameter_1' Textbox component
#         "",  # str in 'System Prompt' Textbox component
#         0.8,
#         100,
#         api_name="/chat"
#     )
#     tasks.append(task)

# print([])
