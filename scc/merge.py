import json

# Function to read a JSON file and return its contents as a list of dictionaries


def read_json_file(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
    return data

# Function to remove duplicates from a list of dictionaries based on a specific key


def remove_duplicates(data, key):
    seen = set()
    result = []
    for item in data:
        if item[key] not in seen:
            seen.add(item[key])
            result.append(item)
    return result


# File paths for the three JSON files
file1_path = 'scc.json'
file2_path = 'scc-vm.json'
file3_path = 'scc-latest.json'

# Read the contents of the three JSON files
data1 = read_json_file(file1_path)
data2 = read_json_file(file2_path)
data3 = read_json_file(file3_path)

# Combine the data from all three files into a single list
merged_data = data1 + data2 + data3

# Specify the key based on which duplicates should be removed (e.g., 'id')
key_to_check = 'headline'

# Remove duplicates based on the specified key
unique_data = remove_duplicates(merged_data, key_to_check)

# Write the unique data to a new JSON file
output_file_path = 'merged_and_deduplicated.json'
with open(output_file_path, 'w') as output_file:
    json.dump(unique_data, output_file, indent=4)

print(f"Merged and deduplicated data saved to {output_file_path}")
