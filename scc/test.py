

import re

import requests
from bs4 import BeautifulSoup
from git import Tree

from utils import convert_table

r = requests.get(
    'https://www.scconline.com/blog/post/2023/09/22/cognizance-for-extension-of-limitation-in-re-operative-effect-of-the-supreme-courts-order-dated-10-1-2022-a-case-comment/')


# text = re.sub(r'<sup>.+?</sup>', '', str(r.text))


soup = BeautifulSoup(r.text)

div = soup.find(class_='entry-content').find('div', class_='')
# Find all <sup> tags
sup_tags = soup.find_all('sup')

# Modify the content inside <sup> tags
for sup in sup_tags:
    sup.string = f"[{sup.string}]"

all_text = ''


# jp-relatedposts
# sharedaddy sd-sharing-enabled
# twitter-share

# print(div.prettify())
# clasess = d.get('class', [])

# if any(class_name in clasess for class_name in ["jp-relatedposts", "sharedaddy", "twitter-share"]):
# continue

# print(d, clasess)
for d in div.find_all(recursive=False):
    if d.name == 'table':
        all_text = all_text + '\n' + convert_table(d)

    elif d.find('table'):
        all_text = all_text + '\n' + convert_table(d.find('table'))

    else:
        all_text = all_text + "\n" + d.get_text()


print(all_text)
print('----')
print([p.get_text() for p in soup.find(
    class_='entry-content').find_all('p', recursive=False)])
