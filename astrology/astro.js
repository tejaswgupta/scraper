var fs = require("fs");

// fetch("https://api.kundali.astrotalk.com/v1/combined/general", {
//   body: '{"detail":{"name":"cwecec","gender":"Male","hour":15,"min":22,"sec":10,"day":28,"month":2,"year":2000,"tzone":5.5,"lon":80.34975,"lat":26.46523,"place":"Kanpur, Kanpur, Uttar Pradesh","userId":"","accuracy":3,"weekday":2,"languageId":1,"seq":[]},"languageId":1}',
//   cache: "default",
//   credentials: "omit",
//   headers: {
//     Accept: "application/json, text/plain, */*",
//     "Accept-Language": "en-IN,en-US;q=0.9,en;q=0.8",
//     "Content-Type": "application/json",
//     "User-Agent":
//       "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.0 Safari/605.1.15",
//   },
//   method: "POST",
//   mode: "cors",
//   redirect: "follow",
//   referrer: "https://astrotalk.com/",
//   referrerPolicy: "strict-origin-when-cross-origin",
// });

async function main() {
  const a = await fetch(
    "https://api.kundali.astrotalk.com/v1/combined/general",
    {
      body: '{"detail":{"name":"dcwdcd","gender":"Male","hour":17,"min":15,"sec":0,"day":28,"month":2,"year":2000,"tzone":5.5,"lon":80.34975,"lat":26.46523,"place":"Kanpur, Uttar Pradesh, Kanpur","userId":"","accuracy":3,"weekday":2,"languageId":1,"seq":[]},"languageId":1}',
      cache: "default",
      credentials: "omit",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Accept-Language": "en-IN,en-US;q=0.9,en;q=0.8",
        "Content-Type": "application/json",
        "User-Agent":
          "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.0 Safari/605.1.15",
      },
      method: "POST",
      mode: "cors",
      redirect: "follow",
      referrer: "https://astrotalk.com/",
      referrerPolicy: "strict-origin-when-cross-origin",
    }
  );

  const b = await a.json();
  fs.writeFile("astro.json", JSON.stringify(b["data"]), "utf8", (err) => {
    if (err) throw err;
    console.log("The file has been saved!");
  });
  console.log(b);
}

async function read() {
  const f = fs.readFileSync("astro.json", "utf-8", (err, data) => {
    return data;
  });

  // planet_ext_dtls;
  // all_mahadasha;
  const data = JSON.parse(f);
  const planets = data["data"]["planet_ext_dtls"];

  const active = planets.find((obj) => obj.name === "SATURN");
  const active1 = planets.find((obj) => obj.name === "JUPITER");
  console.log(planets);
}

// read();
main();
