

import json

strings_to_check = ['Home/Articles', 'Home/Know the Law', 'Home/Consumer Cases',
                    'Home/Law Schools /Law School Articles', 'Home/Law Firms /Law Firm Articles']

with open('livelaw.json', 'r') as file:
    data = json.load(file)
    s = []
    for d in data:
        text = d['tag']

        if any(string in text for string in strings_to_check):
            s.append(d)

        # last_slash_index = text.rfind('/')
        # if last_slash_index != -1
        #     result = text[:last_slash_index + 1]
        # else:
        #     result = text

        # s.add(result)

    # print(s)
    with open('main.json', 'w') as file:
        json.dump(s, file)


# https://www.livelaw.in/articles Home/Articles
# https://www.livelaw.in/know-the-law Home/Know the Law
# https://www.livelaw.in/consumer-cases Home/Consumer Cases
# https://www.livelaw.in/tax-cases
# https://www.livelaw.in/arbitration-cases
# https://www.livelaw.in/lawschool/articles Home/Law Schools /Law School Articles
# https://www.livelaw.in/law-firms/law-firm-articles- Home/Law Firms /Law Firm Articles
