import asyncio
import datetime
import json
import os
import time
import xml.etree.ElementTree as ET

import requests
from bs4 import BeautifulSoup
from pdfminer.high_level import extract_text

# Change this to your directory containing the XML files
xml_directory = 'xmls'


def extract_urls_from_xml(xml_file):
    with open(os.path.join(xml_directory, xml_file), 'r', encoding='utf-8') as file:
        soup = BeautifulSoup(file, 'xml')  # Parse the XML using BeautifulSoup
        loc_tags = soup.find_all('loc')  # Find all <loc> tags
        # Extract the URLs and remove leading/trailing spaces
        urls = [loc.text.strip()
                for loc in loc_tags if not loc.text.strip().endswith('.jpg')]
    return urls


# Issue with updated_max which needs to be passed as a parameter
file_name = 'livelaw.json'


def update_file(data_list):
    existing_data = []

    with open(file_name, 'r', encoding='utf-8') as json_file:
        existing_data = json.load(json_file)

    # Combine existing data with new data
    combined_data = existing_data + data_list

    # Write the combined data back to the JSON file
    with open(file_name, 'w', encoding='utf-8') as json_file:
        json.dump(combined_data, json_file, ensure_ascii=False, indent=4)


async def get_data(url):
    res = requests.get(url, headers={
        'Cookie': 'IGNORE_INTERSTITIAL=1; _xhr_verified_=1; source=post_page---------------------------; _ga_store=!%2540%2523%2524%2525%255E%2526*%2540%2523%2524%255E%2525%2526(*)(%2526%2524%2525%2526%255E*%253D%253D%2523%2523172313074; HOCAL_WEBSITE_USER=XviI7a1QC0XLZ2wN8nvmaafEpvVx2I2oZX2-8DdbV7L-Qiy57glA5XwZdbXH0_fu; _SHOWN_POPUP_=true; _ga_external_value_=1'
    }, timeout=30)
    soup = BeautifulSoup(res.text, 'lxml')
    title = soup.find('h1', class_='heading_for_first').get_text() if soup.find(
        'h1', class_='heading_for_first') is not None else ""
    print(title)
    data = soup.find(class_='details-story-wrapper').get_text() if soup.find(
        class_='details-story-wrapper') is not None else ""

    tag = soup.find(class_='breadcrumbs_list').get_text() if soup.find(
        class_='breadcrumbs_list') is not None else ""

    pdf = soup.find('a', href=lambda href: href and 'pdf_upload' in href)
    if pdf:
        pdf_url = pdf.get('href')
    else:
        pdf_url = None

    # if pdf:
    #     pdf_url = pdf.get('href')
    #     pdf_filename = os.path.join("pdfs", os.path.basename(pdf_url))
    #     response = requests.get(pdf_url)
    #     if response.status_code == 200:
    #         with open(pdf_filename, "wb") as pdf_file:
    #             pdf_file.write(response.content)
    #         judgement = extract_text(pdf_filename)
    #     else:
    #         print(
    #             f"Failed to download PDF from '{pdf_url}' (status code {response.status_code})")

    news_item = {
        'headline': title,
        'subheadline': '',
        'data': data,
        'tag': tag,
        'judgement_url': pdf_url,
    }

    return news_item


async def process_batch(batch):
    tasks = []
    for element in batch:
        data = get_data(element)
        tasks.append(data)
    return await asyncio.gather(*tasks)


async def main():
    xml_files = [f for f in os.listdir(xml_directory) if f.endswith('.xml')]
    urls = []

    for xml_file in xml_files:
        urls.extend(extract_urls_from_xml(xml_file))

    print('https://www.livelaw.in/law-firms/deals/kochhar-co-advises-sterlite-power-on-a-inr-207-billion-mega-financing-deal-165907' in urls)
    print(len(urls))
    # for i in range(0, len(urls), 30):
    #     batch = urls[i:i + 30]
    #     batch_result = await process_batch(batch)
    #     update_file(batch_result)


if __name__ == '__main__':
    os.makedirs('pdfs', exist_ok=True)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
