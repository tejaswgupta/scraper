import csv
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import pandas as pd

project_urls = [
    "https://www.lawweb.in",
    "https://lawbhoomi.com/law-notes/",
    "https://leggerhythms.org/category/law-articles/",
    "https://www.vkeel.com/legal-blog",
    "https://theamikusqriae.com/legal-articles/",
    "https://spicyip.com",
    "https://iprlawindia.org/blog/",
    "https://razorpay.com/learn/",
    "https://www.irccl.in/blog",
    "https://indiacorplaw.in",
    "https://cyberblogindia.in/blog/",
    "https://www.legalbites.in/topics/articles",
    "https://www.legalraasta.com/blog/",
    "https://www.lawyered.in/legal-disrupt/",
    "https://lawbriefcase.com/library-blog/",
    "https://lawctopus.com/clatalogue/clat-pg/",
    "https://www.juscorpus.com/category/blogs/",
    "https://strictlylegal.in/blog/",
    "https://shoneekapoor.com/",
    "https://www.lawinsider.in",
    "https://barandbench.com",
    "https://blog.tax2win.in",
    "https://lawrato.com/free-legal-advice | https://lawrato.com/legal-documents/",
    "https://www.etmoney.com/learn/",
    "https://taxguru.in/filters/?cats=rbi&type=articles&court=0&pyear=0&pmonth=&filters=Y",
    "https://prsindia.org/billtrack",
    "https://www.barelaw.in/legal-drafts/",
    "https://www.barelaw.in/indian-courts-2/",
    "https://www.pathlegal.in/",
    "https://blog.ipleaders.in",
    "https://www.indialegallive.com/",
    "https://lawrato.com/indian-kanoon/law-guides",
    "https://www.scconline.com/blog/post/category/op-ed/legal-analysis/"
    "https://www.legalserviceindia.com/articles/articles.html",
]


# Make a copy of the original project_urls list
project_urls_copy = project_urls.copy()

# Email configuration
smtp_server = 'smtp.office365.com'  # Replace with your SMTP server
# Replace with the SMTP server's port (587 for TLS, 465 for SSL)
smtp_port = 587
smtp_username = 'hi@lamarr.tech'  # Replace with your SMTP username
smtp_password = 'Buk79686'  # Replace with your SMTP password
sender_email = 'hi@lamarr.tech'  # Replace with your email address


df = pd.read_csv("emails.csv")['Email'].drop_duplicates()
df_sent = pd.read_csv('email_assignments.csv')['Email']

emails = list(set(df).difference(set(df_sent)))


email_to_url_mapping = {}


for email in emails:
    if not project_urls_copy:

        project_urls_copy = project_urls.copy()  # Reset to the original list
    url = project_urls_copy.pop(0)  # Get the first URL from the list
    email_to_url_mapping[email] = url


for email, url in email_to_url_mapping.items():
    subject = 'Lamarr Data Science: Next Steps'
    message = f"""I hope this message finds you well. Thank you for expressing your interest in the Data Science position at Lamarr. We were very impressed by your background and qualifications.

To move forward in the hiring process, we would like to invite you to complete a short screening project at your earliest convenience. Completing this project will allow us to further assess your skills and experience.

Once you have completed the screening project, we can schedule a call to discuss the next steps. We are looking forward to reviewing your work and learning more about how you could contribute to our data science team.

Thank you for your interest in this opportunity, and we appreciate your dedication to the application process. Please feel free to reach out if you have any questions or need any assistance.

Website to scrape: {url}
Guidelines: https://1drv.ms/b/s!AsJHPy_BuInhh1c7zo7BAQpsM6Za?e=u5Bu1F

Best regards,
Lamarr Team
    """

    # Create an SMTP server connection
    try:
        server = smtplib.SMTP(smtp_server, smtp_port)
        server.starttls()  # Use TLS for security
        server.login(smtp_username, smtp_password)

        # Create the email message
        msg = MIMEMultipart()
        msg['From'] = sender_email
        msg['To'] = email
        msg['Subject'] = subject

        msg.attach(MIMEText(message, 'plain'))

        # Send the email
        server.sendmail(sender_email, email, msg.as_string())
        print(f"Email sent to {email} with URL: {url}")

        # Close the server connection
        server.quit()

        # Log the assignment to a CSV file
        with open('email_assignments.csv', 'a', newline='') as csvfile:
            fieldnames = ['Email', 'Assigned_URL']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writerow({'Email': email, 'Assigned_URL': url})

    except Exception as e:
        print(f"Failed to send email to {email}: {str(e)}")
