import json

# Load the jsonl file  
with open('merged_data.json', 'r') as file:  
    data = file.readlines()  
  
# This set will store unique json objects  
unique_data = set()  
  
for item in data:  
    # Load json object  
    obj = json.loads(item)  
      
    # We create a tuple from the dictionary to make it hashable  
    # We only include the 'human' and 'ai' keys as those are the ones we're interested in  
    hashable_obj = (obj['human'], obj['ai'])  
      
    # Add the object to the set. Duplicates will be ignored automatically  
    unique_data.add(hashable_obj)  
  
# Now we can convert the unique data back to json and save it  
with open('output.jsonl', 'w') as file:  
    for item in unique_data:  
        # Convert the tuple back to a dictionary  
        obj = {'human': item[0], 'ai': item[1]}  
          
        # Dump the dictionary as a json object  
        json_obj = json.dumps(obj)  
          
        # Write the json object to the file  
        file.write(json_obj + '\n')  
