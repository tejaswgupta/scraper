import asyncio
import json

import requests
from bs4 import BeautifulSoup

file_name = 'lawbhoomi.json'


def update_file(data_list):
    existing_data = []

    with open(file_name, 'r', encoding='utf-8') as json_file:
        existing_data = json.load(json_file)

    # Combine existing data with new data
    combined_data = existing_data + data_list

    # Write the combined data back to the JSON file
    with open(file_name, 'w', encoding='utf-8') as json_file:
        json.dump(combined_data, json_file, ensure_ascii=False, indent=4)


async def get_data(url):
    res = requests.get(url)
    soup = BeautifulSoup(res.text, 'lxml')
    title = soup.find('h3', class_='entry-title').get_text() if soup.find(
        'h3', class_='entry-title') is not None else ""
    print(title)
    elements = soup.find(class_='entry-content')
    current_data = elements.get_text()
    external_cite = soup.find(
        class_='entry-content', href=lambda href: href and href.startswith('https://www.lawweb.in/'))

    if external_cite:
        external_data = get_data(external_cite.get('href'))
        current_data += external_data

    news_item = {
        'headline': title,
        'subheadline': '',
        'data': current_data,
    }

    return news_item


async def main(url):
    i = 2

    while True:
        response = requests.get(url)
        soup = BeautifulSoup(response.text)

        a_tags = soup.find_all
        print(i, len(elements))

        tasks = []
        for element in elements:
            data = get_data(element)
            tasks.append(data)

        data_list = await asyncio.gather(*tasks)
        print(data_list)
        # update_file(data_list)

        if (len(elements) < 10):
            break

        i += 1
        break


urls = ['https://lawbhoomi.com/administrative-law-notes-with-important-case-laws-and-study-materials/', '']
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
