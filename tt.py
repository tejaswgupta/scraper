import json
import os
from itertools import islice
from pathlib import Path


def merge_json_files(input_directory, output_file):
    merged_data = []

    # Iterate through each folder in the input directory
    for folder_name in os.listdir(input_directory):
        folder_path = os.path.join(input_directory, folder_name)

        # Check if the folder is actually a directory
        if os.path.isdir(folder_path):
            # Iterate through JSON files in the folder
            for filename in os.listdir(folder_path):
                if filename.endswith(".json"):
                    file_path = os.path.join(folder_path, filename)
                    print('processing', file_path)

                    # Load the JSON data from the file
                    with open(file_path, "r", encoding="utf-8") as json_file:
                        data = json.load(json_file)
                        for d in data:
                            if len(d['data']) > 500:
                                merged_data.append(d)

    with open(output_file, "w", encoding="utf-8") as output_json:
        json.dump(merged_data, output_json, ensure_ascii=False, indent=4)


# Replace with your input directory path
input_directory = "/Users/tejasw/Downloads/scraper-law"
output_file = "merged_data.json"  # Replace with your desired output file name
# merge_json_files(input_directory, output_file)


# with open('merged_data.json') as file:
#     json_data = json.load(file)
#     for d in json_data:
#         data = d['data']
#         if len(data) > 5000:
#             print(data)
# dataset.append({
#     'text': base_prompt.format(human=title,ai=data)
# })

# i = 0
# for d in json_data:
#     if (len(d['data']) > 500):
#         i += 1
# print(i)

