# import requests
import json
import os
import xml.etree.ElementTree as ET

import requests
from bs4 import BeautifulSoup

# res = requests.get(
#     'https://blog.ipleaders.in/overview-on-doctrine-of-res-judicata/')
# soup = BeautifulSoup(res.text, 'html.parser')

# elements = soup.find(class_='td-post-content').children

# text = ''
# for e in elements:
#     if not hasattr(e, 'get'):  # Check if it's a NavigableString
#         continue
#     if e.get('id') is None:
#         text += e.get_text()

# print(text)


u1 = "https://kanpurnagar.dcourts.gov.in/wp-admin/admin-ajax.php?es_ajax_request=1&action=get_order_pdf&input_strings=eyJjaW5vIjoiVVBLTjAxMDA0NzkwMjAxMSIsIm9yZGVyX25vIjoxLCJvcmRlcl9kYXRlIjoiMjAyMi0wOS0wMyJ9"
u2 = "https://kanpurnagar.dcourts.gov.in/wp-admin/admin-ajax.php?es_ajax_request=1&action=get_order_pdf&input_strings=eyJjaW5vIjoiVVBLTjAxMDA4MjU0MjAyMSIsIm9yZGVyX25vIjoxLCJvcmRlcl9kYXRlIjoiMjAyMi0wOS0wMyJ9"
# res = requests.get(u2)
# print(res.text)

# with open('news_data.json', 'r') as file:
#     a = json.load(file)
#     print(a)


# LEGAL SERVICES INDIA
# url = 'https://www.legalserviceindia.com/articles/articles.html'

# headers = {
#     "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
#     "Accept-Language": "en-IN,en-US;q=0.9,en;q=0.8",
#     "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.0 Safari/605.1.15"
# }

# response = requests.get(url, headers=headers)
# soup = BeautifulSoup(response.text)
# e = soup.find_all('a', href=lambda href: href and href.startswith(
#     '/legal/'), class_='pagelink')

# print(len([a.get('href') for a in e]))


# LIVELAW
# urls = [
#     'https://www.livelaw.in/newsletter/married-couple-can-seek-protection-merely-on-the-ground-that-marriage-is-void-or-invalid-ph-hc-148899']

# for url in urls:
#     res = requests.get(url, headers={
#         'Cookie': 'IGNORE_INTERSTITIAL=1; _xhr_verified_=1; source=post_page---------------------------; _ga_store=!%2540%2523%2524%2525%255E%2526*%2540%2523%2524%255E%2525%2526(*)(%2526%2524%2525%2526%255E*%253D%253D%2523%2523172313074; HOCAL_WEBSITE_USER=XviI7a1QC0XLZ2wN8nvmaafEpvVx2I2oZX2-8DdbV7L-Qiy57glA5XwZdbXH0_fu; _SHOWN_POPUP_=true; _ga_external_value_=1'
#     })
#     soup = BeautifulSoup(res.text, 'lxml')
#     print(soup)
#     pdf = soup.find('a', href=lambda href: href and 'pdf_upload' in href)
#     tag = soup.find(class_='breadcrumbs_list').get_text()

#     print(tag)
#     # el = soup.find('ul', class_='breadcrumbs_list').find_all('a')
#     # for a_tag in el:
#     #     if "/lawschool/articles" in a_tag.get('href', ''):
#     #         print('true for', a_tag)
#     # print(el)

# i = 480
# url = "https://blog.ipleaders.in/wp-admin/admin-ajax.php?td_theme_name=Newspaper&v=12.5"

# headers = {
#     "Accept": "*/*",
#     "Accept-Language": "en-IN,en-US;q=0.9,en;q=0.8",
#     "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
#     "newrelic": "eyJ2IjpbMCwxXSwiZCI6eyJ0eSI6IkJyb3dzZXIiLCJhYyI6IjI5NzQ3NTMiLCJhcCI6IjEwOTk5OTM5ODQiLCJpZCI6IjU0MmMzNDcyN2EzOWYxMmQiLCJ0ciI6IjlhYjMzNDAwMTYwOGUwMWJjYTVmMGQ3NzNlNmVjNzAwIiwidGkiOjE2OTM5MjkwMDM3MDl9fQ==",
#     "traceparent": "00-9ab334001608e01bca5f0d773e6ec700-542c34727a39f12d-01",
#     "tracestate": "2974753@nr=0-1-2974753-1099993984-542c34727a39f12d----1693929003709",
#     "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.0 Safari/605.1.15",
#     "X-NewRelic-ID": "Vg8AVVFWCxABVFhbDgYDUFQF",
#     "X-Requested-With": "XMLHttpRequest"
# }

# payload = {
#     "action": "td_ajax_block",
#     "td_atts": '{"custom_title":"","limit":"10","ajax_pagination":"infinite","block_type":"td_block_5","separator":"","custom_url":"","block_template_id":"","m3_tl":"","post_ids":"","category_id":"","taxonomies":"","category_ids":"","in_all_terms":"","tag_slug":"","autors_id":"","installed_post_types":"","include_cf_posts":"","exclude_cf_posts":"","sort":"","linked_posts":"","favourite_only":"","offset":"","open_in_new_window":"","show_modified_date":"","time_ago":"","time_ago_add_txt":"ago","time_ago_txt_pos":"","el_class":"","td_ajax_filter_type":"","td_ajax_filter_ids":"","td_filter_default_txt":"All","td_ajax_preloading":"","f_header_font_header":"","f_header_font_title":"Block header","f_header_font_settings":"","f_header_font_family":"","f_header_font_size":"","f_header_font_line_height":"","f_header_font_style":"","f_header_font_weight":"","f_header_font_transform":"","f_header_font_spacing":"","f_header_":"","f_ajax_font_title":"Ajax categories","f_ajax_font_settings":"","f_ajax_font_family":"","f_ajax_font_size":"","f_ajax_font_line_height":"","f_ajax_font_style":"","f_ajax_font_weight":"","f_ajax_font_transform":"","f_ajax_font_spacing":"","f_ajax_":"","f_more_font_title":"Load more button","f_more_font_settings":"","f_more_font_family":"","f_more_font_size":"","f_more_font_line_height":"","f_more_font_style":"","f_more_font_weight":"","f_more_font_transform":"","f_more_font_spacing":"","f_more_":"","m3f_title_font_header":"","m3f_title_font_title":"Article title","m3f_title_font_settings":"","m3f_title_font_family":"","m3f_title_font_size":"","m3f_title_font_line_height":"","m3f_title_font_style":"","m3f_title_font_weight":"","m3f_title_font_transform":"","m3f_title_font_spacing":"","m3f_title_":"","m3f_cat_font_title":"Article category tag","m3f_cat_font_settings":"","m3f_cat_font_family":"","m3f_cat_font_size":"","m3f_cat_font_line_height":"","m3f_cat_font_style":"","m3f_cat_font_weight":"","m3f_cat_font_transform":"","m3f_cat_font_spacing":"","m3f_cat_":"","m3f_meta_font_title":"Article meta info","m3f_meta_font_settings":"","m3f_meta_font_family":"","m3f_meta_font_size":"","m3f_meta_font_line_height":"","m3f_meta_font_style":"","m3f_meta_font_weight":"","m3f_meta_font_transform":"","m3f_meta_font_spacing":"","m3f_meta_":"","ajax_pagination_next_prev_swipe":"","ajax_pagination_infinite_stop":"","css":"","tdc_css":"","td_column_number":2,"header_color":"","color_preset":"","border_top":"","class":"tdi_6","tdc_css_class":"tdi_6","tdc_css_class_style":"tdi_6_rand_style"}',
#     "td_block_id": "tdi_6",
#     "td_column_number": 2,
#     "td_current_page": i,
#     "block_type": "td_block_5",
#     "td_filter_value": "",
#     "td_user_action": "",
#     "td_magic_token": "83d6e89144"
# }

# # url_acts = f"https://blog.ipleaders.in/category/law-notes/law-of-torts-complete-reading-material/page/{i}/"
# response = requests.post(url, data=payload, headers=headers)

# soup = BeautifulSoup(json.loads(response.text)['td_data'])
# # print(soup)

# elements = [element.find('a').get('href')
#             for element in soup.find_all('h3', 'entry-title')]
# print(i, (elements))


# # Send a GET request to the sitemap index URL
# response = requests.get('https://blog.ipleaders.in/post-sitemap.xml')
# with open('test-1.xml', 'wb') as xml_file:
#     xml_file.write(response.content)


# Send a GET request to the sitemap index URL
# response = requests.get('https://blog.ipleaders.in/sitemap_index.xml')
# print(response.text)
# if response.status_code == 200:
#     # Parse the sitemap index content
#     soup = BeautifulSoup(response.text, 'html.parser')

#     # Find all <loc> tags in the sitemap index
#     sitemap_links = soup.find_all('loc')

#     # Create a directory to save the downloaded XML files
#     os.makedirs('downloaded_xml', exist_ok=True)

#     # Download XML files listed in the sitemap index
#     for link in sitemap_links:
#         xml_url = link.text
#         xml_filename = xml_url.split('/')[-1]
#         xml_filepath = os.path.join('downloaded_xml', xml_filename)

#         # Download the XML file
#         xml_response = requests.get(xml_url)

#         if xml_response.status_code == 200:
#             with open(xml_filepath, 'wb') as xml_file:
#                 xml_file.write(xml_response.content)
#                 print(f'Downloaded: {xml_filepath}')
#         else:
#             print(f'Failed to download: {xml_url}')

# else:
#     print('Failed to fetch the sitemap index:')


# Change this to your directory containing the XML files
xml_directory = 'downloaded_xml'

# List all XML files in the directory
xml_files = [f for f in os.listdir(xml_directory) if f.endswith('.xml')]

# Function to extract URLs from an XML file


def extract_urls_from_xml(xml_file):
    with open(os.path.join(xml_directory, xml_file), 'r', encoding='utf-8') as file:
        soup = BeautifulSoup(file, 'xml')  # Parse the XML using BeautifulSoup
        loc_tags = soup.find_all('loc')  # Find all <loc> tags
        # Extract the URLs and remove leading/trailing spaces
        urls = [loc.text.strip()
                for loc in loc_tags if not loc.text.strip().endswith(('.png', '.jpg', '.jpeg'))]
    return urls


# Iterate through each XML file and extract URLs
i = 0
for xml_file in xml_files:
    urls = extract_urls_from_xml(xml_file)

    # Print the extracted URLs for the current XML file
    i += len(urls)

print(i)
