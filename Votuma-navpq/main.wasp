app Votuma {
  wasp: {
    version: "^0.11.1"
  },
  title: "Votuma",
  client: {
    rootComponent: import { Layout } from "@client/Layout.jsx",
  },
  db: {
    prisma: {
      clientPreviewFeatures: ["extendedWhereUnique"]
    }
  },
  auth: {
    userEntity: User,
    methods: {
      usernameAndPassword: {}
    },
    onAuthFailedRedirectTo: "/login",
    onAuthSucceededRedirectTo: "/"
  },
}

route LoginRoute { path: "/login", to: LoginPage }
page LoginPage {
  component: import Login from "@client/pages/auth/Login.jsx"
}
route SignupRoute { path: "/signup", to: SignupPage }
page SignupPage {
  component: import Signup from "@client/pages/auth/Signup.jsx"
}

entity User {=psl
    id        Int        @id @default(autoincrement())
    username  String     @unique
    password  String
    questions Question[]
psl=}

entity Question {=psl
    id        Int        @id @default(autoincrement())
    title     String
    content   String
    responses Response[]
    user      User       @relation(fields: [userId], references: [id])
    userId    Int
psl=}

entity Response {=psl
    id         Int      @id @default(autoincrement())
    content    String
    rating     Int      @default(0)
    question   Question @relation(fields: [questionId], references: [id])
    questionId Int
psl=}

action createQuestion {
  fn: import { createQuestion } from "@server/actions.js",
  entities: [Question]
}

action createResponse {
  fn: import { createResponse } from "@server/actions.js",
  entities: [Response, Question]
}

action rateResponse {
  fn: import { rateResponse } from "@server/actions.js",
  entities: [Response]
}

action regenerateResponse {
  fn: import { regenerateResponse } from "@server/actions.js",
  entities: [Response]
}

query getUserQuestions {
  fn: import { getUserQuestions } from "@server/queries.js",
  entities: [Question]
}

query getQuestionResponses {
  fn: import { getQuestionResponses } from "@server/queries.js",
  entities: [Question, Response]
}

route HomePageRoute { path: "/", to: HomePage }
page HomePage {
  component: import { Home } from "@client/pages/Home.jsx",
  authRequired: true
}

route QuestionRoute { path: "/question/:questionId", to: QuestionPage }
page QuestionPage {
  component: import { Question } from "@client/pages/Question.jsx",
  authRequired: true
}