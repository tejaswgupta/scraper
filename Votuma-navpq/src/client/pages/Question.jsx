import React from 'react';
import { useParams, Link } from 'react-router-dom';
import { useQuery } from '@wasp/queries';
import { useAction } from '@wasp/actions';
import getQuestionResponses from '@wasp/queries/getQuestionResponses';
import createResponse from '@wasp/actions/createResponse';
import rateResponse from '@wasp/actions/rateResponse';
import regenerateResponse from '@wasp/actions/regenerateResponse';

export function Question() {
  const { questionId } = useParams();

  const { data: responses, isLoading, error } = useQuery(getQuestionResponses, { questionId });
  const createResponseFn = useAction(createResponse);
  const rateResponseFn = useAction(rateResponse);
  const regenerateResponseFn = useAction(regenerateResponse);

  if (isLoading) return 'Loading...';
  if (error) return 'Error: ' + error;

  const handleCreateResponse = () => {
    createResponseFn({ questionId, content: '...' });
  };

  const handleRateResponse = (responseId, rating) => {
    rateResponseFn({ responseId, rating });
  };

  const handleRegenerateResponse = (responseId) => {
    regenerateResponseFn({ responseId });
  };

  return (
    <div className='p-4'>
      {/* Question details */}
      <h2>Question Details</h2>
      {/* List of responses */}
      <h3>Responses</h3>
      {responses.map((response) => (
        <div key={response.id} className='response'>
          <p>{response.content}</p>
          <div className='response-rating'>
            <button onClick={() => handleRateResponse(response.id, 1)}>Thumbs Up</button>
            <button onClick={() => handleRateResponse(response.id, -1)}>Thumbs Down</button>
          </div>
          <button onClick={() => handleRegenerateResponse(response.id)}>Regenerate</button>
        </div>
      ))}
      {/* Create response form */}
      <h3>Create Response</h3>
      <form onSubmit={handleCreateResponse}>
        <input type='text' name='content' placeholder='Enter your response' />
        <button type='submit'>Submit</button>
      </form>
      {/* Thumbs up/down and regenerate buttons for each response */}
    </div>
  );
}