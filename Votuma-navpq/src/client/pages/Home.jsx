import { useAction } from "@wasp/actions";
import createQuestion from "@wasp/actions/createQuestion";
import { useQuery } from "@wasp/queries";
import getUserQuestions from "@wasp/queries/getUserQuestions";
import { Link } from "react-router-dom";

export function Home() {
  const { data: questions, isLoading, error } = useQuery(getUserQuestions);
  const createQuestionFn = useAction(createQuestion);

  if (isLoading) return "Loading...";
  if (error) return "Error: " + error;

  const handleCreateQuestion = () => {
    createQuestionFn({
      title: "n_title",
    });
  };

  return (
    <div className="p-4">
      <div className="flex justify-between mb-4">
        <h1 className="text-2xl font-bold">My Questions</h1>
        <button
          onClick={handleCreateQuestion}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        >
          Create New Question
        </button>
      </div>
      {questions.map((question) => (
        <div key={question.id} className="bg-gray-100 p-4 mb-4 rounded-lg">
          <h2 className="text-xl font-bold">{question.title}</h2>
          <p className="mt-2">{question.content}</p>
          <div className="mt-4">
            <Link
              to={`/question/${question.id}`}
              className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded"
            >
              View Question
            </Link>
          </div>
        </div>
      ))}
    </div>
  );
}
