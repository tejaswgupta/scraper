import HttpError from "@wasp/core/HttpError.js";

const generateNewContent = (content) => {
  // Generate new content logic
};

export const createQuestion = async (args, context) => {
  if (!context.user) {
    throw new HttpError(401);
  }

  const question = await context.entities.Question.create({
    data: {
      title: args.title,
      content: args.content,
      userId: context.id,
    },
  });

  return question;
};

export const createResponse = async (args, context) => {
  if (!context.user) {
    throw new HttpError(401);
  }

  const question = await context.entities.Question.findUnique({
    where: { id: args.questionId },
  });
  if (!question) {
    throw new HttpError(404);
  }

  const response = await context.entities.Response.create({
    data: {
      content: generateResponse(question),
      rating: 0,
      question: { connect: { id: question.id } },
    },
  });

  return response;
};

export const rateResponse = async (args, context) => {
  const { responseId, rating } = args;
  const response = await context.entities.Response.findUnique({
    where: { id: responseId },
  });

  if (!response) {
    throw new HttpError(404, "Response not found");
  }

  const updatedResponse = await context.entities.Response.update({
    where: { id: responseId },
    data: { rating },
  });

  return updatedResponse;
};

export const regenerateResponse = async (args, context) => {
  const response = await context.entities.Response.findUnique({
    where: { id: args.responseId },
  });

  const newContent = generateNewContent(response.content);

  return context.entities.Response.update({
    where: { id: args.responseId },
    data: { content: newContent },
  });
};
