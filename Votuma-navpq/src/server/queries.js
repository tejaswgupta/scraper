import HttpError from '@wasp/core/HttpError.js'

export const getUserQuestions = async (args, context) => {
  if (!context.user) throw new HttpError(401);

  return context.entities.Question.findMany({
    where: { userId: context.user.id }
  });
}

export const getQuestionResponses = async ({ questionId }, context) => {
  if (!context.user) { throw new HttpError(401) }
  const responses = await context.entities.Response.findMany({
    where: { questionId },
    include: { question: true }
  })

  return responses;
}