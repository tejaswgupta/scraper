import re

import pandas as pd
from bs4 import BeautifulSoup

from utils import convert_table

# Replace this with your HTML table content
html_table = """
<table cellspacing="0" style="border-collapse:collapse; border:none; width:100.0%">
<thead>
<tr>
<td style="border-color:black; border-style:solid; border-width:1.0pt; height:20.0pt; width:26.7%">
<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>The Draft Personal Data Protection Bill, 2018</strong></span></span></p>
</td>
<td style="border-color:black; border-left:none; border-style:solid; border-width:1.0pt; height:20.0pt; width:22.8%">
<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>The Personal Data Protection Bill, 2019</strong></span></span></p>
</td>
<td style="border-color:black; border-left:none; border-style:solid; border-width:1.0pt; height:20.0pt; width:25.22%">
<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>Recommendations of the Joint Parliamentary Committee</strong></span></span></p>
</td>
<td style="border-color:black; border-left:none; border-style:solid; border-width:1.0pt; height:20.0pt; width:25.28%">
<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>The Digital Personal Data Protection Bill, 2023</strong></span></span></p>
</td>
</tr>
</thead>
<tbody>
<tr>
<td colspan="4" style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:100%">
<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>Scope and Applicability</strong></span></span></p>
</td>
</tr>
<tr>
<td style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:44.35pt; vertical-align:top; width:26.7%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Processing of personal data: (i) within India, (ii) outside India if it is for business carried on, offering of goods and services, or profiling individuals, in India</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:44.35pt; vertical-align:top; width:22.8%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Expands the scope under the 2018 Bill to cover certain anonymised personal data</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:44.35pt; vertical-align:top; width:25.22%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Expands the scope under the 2018 Bill to include processing of non-personal data and anonymised personal data</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:44.35pt; vertical-align:top; width:25.28%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">does not cover offline personal data and non-automated processing</span></span></p>
</li>
</ul>
</div>
</td>
</tr>
<tr>
<td colspan="4" style="border-color:black; border-style:solid; border-top:none; border-width:1.0pt; height:3.15pt; width:100.0%">
<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>Reporting of data breaches</strong></span></span></p>
</td>
</tr>
<tr>
<td style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:3.15pt; vertical-align:top; width:26.7%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Fiduciary to notify the Data Protection Authority about a breach which is likely to cause harm, the Authority will decide whether to notify the data principals or not</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:3.15pt; vertical-align:top; width:22.8%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Same as 2018 Bill</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:3.15pt; vertical-align:top; width:25.22%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">All breaches, regardless of potential harm, must be reported to the Authority, within 72 hours</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:3.15pt; vertical-align:top; width:25.28%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Every personal data breach must be reported to the Data Protection Board of India and each affected data principal, in prescribed manner</span></span></p>
</li>
</ul>
</div>
</td>
</tr>
<tr>
<td colspan="4" style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:100%">
<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>Exemptions from provisions of the Bill for the security of the state, public order, prevention of offences etc.</strong></span></span></p>
</td>
</tr>
<tr>
<td style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:4.5pt; vertical-align:top; width:26.7%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Processing must be authorised pursuant to a law, and in accordance with the procedure established by law, and must be necessary and proportionate</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:4.5pt; vertical-align:top; width:22.8%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">The central government, by order, may exempt agencies where processing is necessary or expedient, subject to certain procedure, safeguards, and oversight</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:4.5pt; vertical-align:top; width:25.22%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Adds that order should specify a procedure, which is fair, just, and reasonable</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:4.5pt; vertical-align:top; width:25.28%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">The central government may exempt by notification; does not require any procedure or safeguards to be specified</span></span></p>
</li>
</ul>
</div>
</td>
</tr>
<tr>
<td colspan="4" style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:9.25pt; vertical-align:top; width:100%">
<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>Right to Data Portability and Right to be Forgotten</strong></span></span></p>
</td>
</tr>
<tr>
<td style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:9.25pt; vertical-align:top; width:26.7%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Data principal will have the right to data portability (to obtain data in interoperable format), and right to be forgotten (to restrict disclosure of personal data over internet)</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:9.25pt; vertical-align:top; width:22.8%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Provided for both rights</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:9.25pt; vertical-align:top; width:25.22%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Provided for both rights</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:9.25pt; vertical-align:top; width:25.28%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Not provided</span></span></p>
</li>
</ul>
</div>
</td>
</tr>
<tr>
<td colspan="4" style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:100%">
<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>Harm from processing of personal data</strong></span></span></p>
</td>
</tr>
<tr>
<td style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:26.7%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Harm includes monetary loss, identity theft, loss of reputation, and unreasonable surveillance&nbsp;</span></span></p>
</li>
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Data fiduciaries to take measures to minimise and mitigate risks of harm</span></span></p>
</li>
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Data principal has a right to seek compensation in the event of harm</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:22.8%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Same as 2018 Bill</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:25.22%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">The central government should have powers to prescribe additional harms</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:25.28%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Not provided</span></span></p>
</li>
</ul>
</div>
<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">&nbsp;</span></span></p>
</td>
</tr>
<tr>
<td colspan="4" style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:100%">
<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>Regulator</strong></span></span></p>
</td>
</tr>
<tr>
<td style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:26.7%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Provides for establishing: (i) the Data Protection Authority of India to regulate the sector, and (ii) the Appellate Tribunal.</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:22.8%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Same as 2018 Bill</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:25.22%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Same as 2018 Bill</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:25.28%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Provides for the Data Protection Board of India, whose primary function is to adjudicate non-compliance;</span></span></p>
</li>
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">TDSAT has been designated as the Appellate Tribunal</span></span></p>
</li>
</ul>
</div>
</td>
</tr>
<tr>
<td colspan="4" style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:100%">
<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif"><strong>Transfer of personal data outside India</strong></span></span></p>
</td>
</tr>
<tr>
<td style="border-bottom:1pt solid windowtext; border-image:initial; border-left:1pt solid windowtext; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:26.7%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Every fiduciary to store at least one serving copy of personal data in India</span></span></p>
</li>
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">May be transferred outside India, if consent provided, to certain permitted countries or under contracts approved by the Authority</span></span></p>
</li>
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Certain critical data can be processed only in India</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:22.8%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">A copy of sensitive personal data should remain in India</span></span></p>
</li>
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Certain sensitive personal data may be transferred only if explicit consent provided, no restriction on other personal data</span></span></p>
</li>
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">On critical personal data, same as 2018 Bill</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:25.22%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Adds that sensitive personal data will not be shared with foreign agencies or government, without prior approval of the central government</span></span></p>
</li>
</ul>
</div>
</td>
<td style="border-bottom:1pt solid windowtext; border-left:none; border-right:1pt solid windowtext; border-top:none; height:10.2pt; vertical-align:top; width:25.28%">
<div style="font-family:&quot;Times New Roman&quot;,serif; font-size:16px; margin-bottom:0cm; margin-left:0cm; margin-right:0cm; margin-top:0cm">
<ul style="list-style-type:square; margin-left:1.3499999999999996px">
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">Removes sensitive and critical personal data classification</span></span></p>
</li>
<li>
<p><span style="font-size:16px"><span style="font-family:Times New Roman,Times,serif">The central government may restrict of personal data to certain countries through notification</span></span></p>
</li>
</ul>
</div>
</td>
</tr>
</tbody>
</table>
"""

# Parse the HTML content
soup = BeautifulSoup(html_table, 'lxml')
html_table = soup.find_all('table')

# Find the table element
# table = soup.find('table')

# table.string = convert_table(table)


# print(soup.prettify())

for table in html_table:
    df = pd.read_html(table.prettify())[0]
    markdown_table = df.to_markdown(index=False)
    # markdown_table = re.sub(r'\s+', ' ', markdown_table)
    table.string = markdown_table

print(markdown_table)
# html_table = """
# <table>
#     <tr>
#         <th>Name</th>
#         <th>Age</th>
#     </tr>
#     <tr>
#         <td>John</td>
#         <td>25</td>
#     </tr>
#     <tr>
#         <td>Jane</td>
#         <td>30</td>
#     </tr>
# </table>
# """
