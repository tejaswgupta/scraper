import requests
from bs4 import BeautifulSoup
from flask import request


def convert_table(table):
    table_data = []
    for row in table.find_all('tr'):
        row_data = [cell.get_text(strip=True)
                    for cell in row.find_all(['td', 'th'])]
        table_data.append(row_data)

    # Remove empty rows (in case the table has rowspan or colspan attributes)
    table_data = [row for row in table_data if any(row)]

    # Convert table data to Markdown format
    markdown_table = '| ' + \
        ' | '.join(table_data[0]) + ' |\n' + '| ' + \
        ' | '.join(['---'] * len(table_data[0])) + ' |\n'
    markdown_table += '\n'.join(['| ' + ' | '.join(row) +
                                ' |' for row in table_data[1:]])

    return markdown_table


if __name__ == '__main__':
    html = """
    <table class="wp-block-table"><tbody><tr><td><strong>Particulars</strong></td><td><strong>Row</strong></td><td><strong>Pre-GST</strong></td><td><strong>Post-GST</strong></td></tr><tr><td>Cost of manufacturing</td><td>A</td><td>70000</td><td>70000</td></tr><tr><td>Customs duty @ 22%(Basic 20% + Surcharge 2%)</td><td>B</td><td>15400</td><td>15400</td></tr><tr><td><strong>Value for VAT/GST calculation</strong></td><td><strong>C=A+B</strong></td><td><strong>85400</strong></td><td><strong>85400</strong></td></tr><tr><td>VAT @14%*/GST @18%</td><td>D</td><td>11956</td><td>15372</td></tr><tr><td><strong>Sale price to the retailer</strong></td><td><strong>E=C+D</strong></td><td><strong>97356</strong></td><td><strong>100772</strong></td></tr><tr><td>Value addition by retailer</td><td>F</td><td>2000</td><td>2000</td></tr><tr><td>VAT@14%*/GST @18%</td><td><strong>G</strong></td><td>280</td><td>360</td></tr><tr><td><strong>Total price charged to the customer</strong></td><td><strong>H = E+F+G</strong></td><td><strong>99636</strong></td><td><strong>103132</strong></td></tr></tbody></table>
    """
    url = 'https://cleartax.in/s/gst-on-iphone'
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'lxml')

    a = convert_table(soup)
    print(a)
