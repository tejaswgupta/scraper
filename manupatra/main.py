import asyncio
import json
import sys

import requests
from bs4 import BeautifulSoup

from utils import convert_table

# Issue with updated_max which needs to be passed as a parameter

file_name = 'manupatra.json'

headers = {
    "Accept": "application/json, text/plain, */*",
    "Accept-Language": "en-IN,en-US;q=0.9,en;q=0.8",
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.0 Safari/605.1.15",
}


def update_file(data_list):
    existing_data = []

    with open(file_name, 'r', encoding='utf-8') as json_file:
        existing_data = json.load(json_file)

    # Combine existing data with new data
    combined_data = existing_data + data_list

    # Write the combined data back to the JSON file
    with open(file_name, 'w', encoding='utf-8') as json_file:
        json.dump(combined_data, json_file, ensure_ascii=False, indent=4)


async def get_data(element, raw_body):

    raw_body['ArticleId'] = element

    print(raw_body)

    res = requests.post(
        'https://articles.manupatra.com/api/Articles/GetDataByCategoryIdDisplayAsync',
        data=json.dumps(raw_body),
        headers=headers,
    ).json()

    if not res:
        return

    soup = BeautifulSoup(res[0]['vFullText'], 'lxml')

    title = res[0]['vTitle']
    print(title)

    try:
        div = soup.find('div')

        sup_tags = soup.find_all('sup')

        for sup in sup_tags:
            sup.string = f"[{sup.string}]"

        if div:
            all_text = ''
            for d in div.find_all(recursive=False):
                if d.name == 'table':
                    all_text = all_text + '\n' + convert_table(d)

                elif d.name == 'footer':
                    continue

                else:
                    all_text = all_text + "\n" + d.get_text()

        news_item = {
            'headline': title,
            'subheadline': '',
            'data': all_text,
        }

        return news_item

    except Exception:
        return {
            'headline': '',
            'subheadline': '',
            'data': '',
        }


async def main(id_id):
    i = 0

    while True:
        raw_body = {"ArticleCategoryID": id_id,
                    "ArticleCategoryName": "",
                    "AuthorName": "",
                    "SubjectId": "",
                    "SubjectName": "",
                    "TitleName": "",
                    "FullText": "",
                    "Tags": "",
                    "JournalId": "",
                    "Firm": "",
                    "NewsLetterId": "",
                    "ByDate": "2023-09-27T06:59:41.198Z",
                    "Year": "", "ArticleId": "",
                    "MailId": "",
                    "PageNumber": i,
                    "VerificationCode": ""
                    }

        response = requests.post(
            "https://articles.manupatra.com/api/Articles/GetDataByCategoryIdAsync",
            headers=headers,
            data=json.dumps(raw_body),
            timeout=30,
        ).json()

        elements = [a['uArticleGuid'] for a in response]

        print(elements, i, len(elements))

        tasks = []
        for element in elements:
            data = get_data(element, raw_body)
            tasks.append(data)

        data_list = await asyncio.gather(*tasks)
        print(data_list)
        update_file(data_list)

        if len(elements) < 10:
            print(f'Breaking for at {i}')
            break

        i += 1


urls = ['1', '4']


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    for url in urls:
        loop.run_until_complete(main(url))
