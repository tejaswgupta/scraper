import json

import requests
from bs4 import BeautifulSoup

url = "https://www.barandbench.com/api/v1/collections/litigation-news?item-type=story&offset={offset}&limit=100"


topics = ['law-policy-news', 'litigation-columns-columns',
          'policy-columns-columns', 'view-point']


def get_data(slug):
    base_url = 'https://www.barandbench.com'
    res = requests.get(base_url + '/' + slug)
    soup = BeautifulSoup(res.text, 'lxml')
    elements = soup.find_all(class_='arr--story-page-card-wrapper')
    texts = " ".join([div.get_text() for div in elements])
    return texts


def main():
    i = 0
    while True:
        data_list = []
        news = requests.get(url.format(offset=i))
        data = json.loads(news.text)

        print(i, len(data['items']))

        if len(data['items']) < 100:
            break

        for n in data['items']:
            headline = n['story']['headline']
            subheadline = n['story']['subheadline']
            slug = n['story']['slug']
            text_data = get_data(slug)
            news_item = {
                'headline': headline,
                'subheadline': subheadline,
                'data': text_data
            }
            data_list.append(news_item)

        i += 100

        existing_data = []

        with open('barandbench.json', 'r', encoding='utf-8') as json_file:
            existing_data = json.load(json_file)

        # Combine existing data with new data
        combined_data = existing_data + data_list

        # Write the combined data back to the JSON file
        with open('barandbench.json', 'w', encoding='utf-8') as json_file:
            json.dump(combined_data, json_file, ensure_ascii=False, indent=4)


main()
# news = requests.get(url)
# print(news.text)

# with open('out.json') as file:
#     a = json.load(file)
#     # headline, subheadline , slug
#     og_text = get_data(a['items'][0]['story']['slug'])
#     cards = a['items'][0]['story']['cards']
#     stories = []
#     for s in cards:
#         stories.extend(s['story-elements'])
#     texts = [t['text'] for t in stories if 'text' in t.keys()]
#     print("".join(texts), og_text)
