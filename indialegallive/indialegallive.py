import asyncio
import json

import requests
from bs4 import BeautifulSoup

file_name = 'indialegallive.json'


def update_file(data_list):
    existing_data = []

    with open(file_name, 'r', encoding='utf-8') as json_file:
        existing_data = json.load(json_file)

    # Combine existing data with new data
    combined_data = existing_data + data_list

    # Write the combined data back to the JSON file
    with open(file_name, 'w', encoding='utf-8') as json_file:
        json.dump(combined_data, json_file, ensure_ascii=False, indent=4)


async def get_data(url):
    res = requests.get(url)
    soup = BeautifulSoup(res.text)
    title = soup.find('h1', class_='tdb-title-text').get_text() if soup.find(
        'h1', class_='tdb-title-text') is not None else ""
    print(title)
    subtitle = soup.find(class_='tdb_single_subtitle').find(
        'p').get_text() if soup.find(class_='tdb_single_subtitle') is not None else ""
    content_el = soup.find(class_='tdb_single_content').find_all(
        'p') if soup.find(class_='tdb_single_content') is not None else []
    text = " ".join([c.get_text() for c in content_el])

    news_item = {
        'headline': title,
        'subheadline': subtitle,
        'data': text,
    }

    return news_item


async def main(b_url):
    i = 350
    while True:
        res = requests.get(f'{b_url}page/{i}')
        soup = BeautifulSoup(res.text, 'lxml')

        elements = soup.find_all(class_='tdb_module_loop')
        print(i, len(elements))

        if (len(elements) < 10):
            break

        tasks = []
        for element in elements:
            slug = element.find(
                'h3', class_='entry-title').find('a').get('href')
            data = get_data(slug)
            tasks.append(data)

        data_list = await asyncio.gather(*tasks)
        update_file(data_list)
        i += 1


# urls = ['https://www.indialegallive.com/is-that-legal-news/',
#         'https://www.indialegallive.com/constitutional-law-news/courts-news/','https://www.indialegallive.com/column-news/']


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(
        main('https://www.indialegallive.com/constitutional-law-news/courts-news/'))
