import asyncio
import json

import requests
from bs4 import BeautifulSoup

# Issue with updated_max which needs to be passed as a parameter

file_name = 'pathlegal.json'


def update_file(data_list):
    existing_data = []

    with open(file_name, 'r', encoding='utf-8') as json_file:
        existing_data = json.load(json_file)

    # Combine existing data with new data
    combined_data = existing_data + data_list

    # Write the combined data back to the JSON file
    with open(file_name, 'w', encoding='utf-8') as json_file:
        json.dump(combined_data, json_file, ensure_ascii=False, indent=4)


async def get_data(url):
    res = requests.get('https://www.pathlegal.in'+url)
    soup = BeautifulSoup(res.text, 'lxml')
    title = soup.find('h1', class_='in-head').get_text() if soup.find(
        'h1', class_='in-head') is not None else ""
    print(title)
    elements = soup.find(class_='blog_content')
    current_data = elements.get_text()

    news_item = {
        'headline': title,
        'subheadline': '',
        'data': current_data,
    }

    return news_item


custom = True


async def main(url):
    i = 1

    while True:
        url_acts = f"{url}&offset2={i}"
        print('calling', url_acts)
        response = requests.get(url_acts, timeout=30)
        soup = BeautifulSoup(response.text)
        elements = [element.find('a').get('href')
                    for element in soup.find_all('div', 'detail')]

        print(url, i, (elements))

        tasks = []
        for element in elements:
            data = get_data(element)
            tasks.append(data)

        data_list = await asyncio.gather(*tasks)
        print(data_list)
        update_file(data_list)

        if len(elements) < 20:
            print(f'Breaking for at {i}')
            break

        i += 1


# "https://www.pathlegal.in/blogs.php?category=Legal",
urls = [
    "https://www.pathlegal.in/blogs.php?category=Judgements"]


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    for url in urls:
        loop.run_until_complete(main(url))
